package webserver;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import leshanserver.MyLeshan;
import leshanserver.model.Piece;


@Controller
@RequestMapping("")
public class IndexController {


    @RequestMapping(value = "/")
    public ModelAndView index(){
        Map<Integer, Piece> pieces = MyLeshan.getInstance().getPieces();

        ModelAndView mv = new ModelAndView("index");
        mv.addObject("pieces", pieces);
        return mv;
    }
}

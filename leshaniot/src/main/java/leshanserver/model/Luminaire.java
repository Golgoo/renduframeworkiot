package leshanserver.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.leshan.core.node.LwM2mResource;
import org.eclipse.leshan.core.response.ObserveResponse;

public class Luminaire extends PieceObject {
	public static final int object_id = 3311 ;
	public static final int on_off_ressource = 5850 ;
	public static final int dimmer_ressource = 5851 ;
	private static final List<Integer> fields_to_obs = new ArrayList<>(Arrays.asList(on_off_ressource, dimmer_ressource));
	
	public boolean on_off ;
	public int dimmer ;
	
	public Luminaire(int instance_id) {
		super(instance_id);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isOn_off() {
		return on_off;
	}

	public int getDimmer() {
		return dimmer;
	}

	@Override
	public void updateValue(ObserveResponse res) {
		switch(res.getContent().getId()) {
		case on_off_ressource:
			on_off = "1".equals(((LwM2mResource)res.getContent()).getValue()) ;
			break;
		case dimmer_ressource:
			dimmer = Integer.valueOf((String) ((LwM2mResource)res.getContent()).getValue());
			break;
		}
	}

	@Override
	public List<Integer> getFieldToObserve() {
		return fields_to_obs ;
	}
	
	@Override
	public int initObjectId() {
		return object_id;
	}

	@Override
	public Map<String, String> getRowValue() {
		Map<String, String> maps = new HashMap<String, String>();
		
		maps.put("on_off", String.valueOf(on_off));
		maps.put("dimmer", String.valueOf(dimmer));
		return maps ;
	}

	@Override
	public String ObjectName() {
		return "Luminaire";
	}

	
}

package leshanserver.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.leshan.core.node.LwM2mResource;
import org.eclipse.leshan.core.response.ObserveResponse;

public class Dimmer extends PieceObject {
	public static final int object_id = 3343;

	public static final int level_ressource = 5548;
	public static final List<Integer> fields_to_observe = new ArrayList<>(Arrays.asList(level_ressource));

	int value;
	
	public Dimmer(int instance_id) {
		super(instance_id);
	}

	public int getValue() {
		return value;
	}

	@Override
	public void updateValue(ObserveResponse res) {
		value = Integer.parseInt((String) ((LwM2mResource) res.getContent()).getValue());
		System.out.println("Value : " + value);
	}

	@Override
	public List<Integer> getFieldToObserve() {
		return fields_to_observe;
	}

	@Override
	public int initObjectId() {
		return object_id;
	}

	@Override
	public Map<String, String> getRowValue() {
		Map<String, String> maps = new HashMap<String, String>();
		
		maps.put("value", String.valueOf(value));
		
		return maps ;
	}

	@Override
	public String ObjectName() {
		return "Dimmer";
	}

}

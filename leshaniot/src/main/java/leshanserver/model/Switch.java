package leshanserver.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.leshan.core.node.LwM2mResource;
import org.eclipse.leshan.core.response.ObserveResponse;

public class Switch extends PieceObject{
	public static final int object_id = 3342;

	public static final int input_state_ressource = 5500;
	public static final List<Integer> fields_to_observe = new ArrayList<>(Arrays.asList(input_state_ressource));

	boolean on_off;

	public Switch(int instance_id) {
		super(instance_id);
	}
	
	@Override
	public void updateValue(ObserveResponse res) {
		on_off = "1".equals(((LwM2mResource)res.getContent()).getValue());
		System.out.println("on_off : " + on_off);
	}

	public boolean isOn_off() {
		return on_off;
	}

	@Override
	public List<Integer> getFieldToObserve() {
		return fields_to_observe;
	}
	
	@Override
	public int initObjectId() {
		return object_id;
	}

	@Override
	public Map<String, String> getRowValue() {
		Map<String, String> maps = new HashMap<String, String>();
		
		maps.put("on_off", String.valueOf(on_off));
		
		return maps ;
	}

	@Override
	public String ObjectName() {
		return "Switch";
	}
}

package leshanserver.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.leshan.core.node.LwM2mResource;
import org.eclipse.leshan.core.response.ObserveResponse;

public class Sensor extends PieceObject{
	public static final int object_id = 3302 ;
	
	public static final int presence_ressource = 5500 ;
	public static final List<Integer> fields_to_observe = new ArrayList<>(Arrays.asList(presence_ressource));

	
	public Sensor(int instance_id) {
		super(instance_id);
	}


	public boolean isDetectingSomething() {
		return detectingSomething;
	}
	

	public boolean detectingSomething ;
	
	@Override
	public void updateValue(ObserveResponse res) {
		detectingSomething = "1".equals(((LwM2mResource)res.getContent()).getValue());
		System.out.println("Movment " + detectingSomething);
	}

	@Override
	public List<Integer> getFieldToObserve() {
		return fields_to_observe ;
	}

	@Override
	public int initObjectId() {
		return object_id;
	}


	@Override
	public Map<String, String> getRowValue() {
		Map<String, String> maps = new HashMap<String, String>();
		
		maps.put("presence", String.valueOf(detectingSomething));
		
		return maps ;
	}


	@Override
	public String ObjectName() {
		return "Sensor";
	}
}

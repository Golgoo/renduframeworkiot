package leshanserver.model;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.leshan.core.request.WriteRequest;
import org.eclipse.leshan.core.response.ObserveResponse;
import org.eclipse.leshan.server.californium.LeshanServer;

public class Piece {

	private LeshanServer server;
	public List<PieceObject> objects = new ArrayList<PieceObject>();

	

	public List<PieceObject> getObjects() {
		return objects;
	}

	public Piece(LeshanServer server) {
		this.server = server;
	}

	private int need_to_light_up(List<PieceObject> switches, List<PieceObject> sensors)
	{
		boolean detecting_something = false ;
		for(PieceObject o : sensors) {
			Sensor s = (Sensor)o;
			if(s.detectingSomething) {
				detecting_something = true; 
				break;
			}
		}
		if(!detecting_something) return 0 ;
		for(PieceObject o : switches) {
			Switch s = (Switch)o ;
			if(s.on_off) return 1 ;
		}
		return 0;
	}
	
	public void update(int object_id, int instance_id, ObserveResponse res) {
		PieceObject o = getObject(object_id, instance_id);
		if(o ==  null) return;
		o.updateValue(res);

		List<PieceObject> switches = getObjects(Switch.object_id);
		List<PieceObject> sensors = getObjects(Sensor.object_id);

		switch (object_id) {
		case Sensor.object_id:
			updateLightsRessourceValue(Luminaire.on_off_ressource, need_to_light_up(switches, sensors));
			break;
		case Dimmer.object_id:
			updateLightsRessourceValue(Luminaire.dimmer_ressource, ((Dimmer)o).value);
			break;
		case Switch.object_id:
			updateLightsRessourceValue(Luminaire.on_off_ressource, need_to_light_up(switches, sensors));
			break;
		default:
			break;
		}
	}

	private void updateLightsRessourceValue(int ressource, int value) {
		for (PieceObject o : getObjects(Luminaire.object_id)){
			if (o.registration != null) {
				try {
					server.send(o.registration,
							new WriteRequest(Luminaire.object_id, o.instance_id, ressource, value));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	
	private List<PieceObject> getObjects(int object_id){
		List<PieceObject> object = new ArrayList<PieceObject>();
		for(PieceObject o : this.objects) {
			
			if(o.object_id != object_id) continue;
			object.add(o);
		}
		
		return object;
	}
	
	private PieceObject getObject(int object_id, int instance_id){
		for(PieceObject o : objects) {
			if(o.object_id != object_id) continue;
			if(o.instance_id != instance_id) continue;
			return o;
		}
		return null;
	}
 
}

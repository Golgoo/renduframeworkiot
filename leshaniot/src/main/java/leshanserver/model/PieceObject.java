package leshanserver.model;

import java.util.List;
import java.util.Map;

import org.eclipse.leshan.core.response.ObserveResponse;
import org.eclipse.leshan.server.registration.Registration;

public abstract class PieceObject {
	public int instance_id ;
	public int object_id;
	
	public int getInstance_id() {
		return instance_id;
	}

	public int getObject_id() {
		return object_id;
	}

	public Registration getRegistration() {
		return registration;
	}
	
	
	public abstract Map<String, String> getRowValue();
	
	public abstract String ObjectName();
	
	public String endpointName() {
		return registration.getEndpoint();
	}

	public PieceObject(int instance_id) {
		this.instance_id = instance_id;
		this.object_id = initObjectId();
	}
	
	public abstract int initObjectId();
	public abstract void updateValue(ObserveResponse res);
	public abstract List<Integer> getFieldToObserve();
	public Registration registration ;
}

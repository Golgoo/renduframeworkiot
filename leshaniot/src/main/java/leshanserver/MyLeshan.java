package leshanserver;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.leshan.core.Link;
import org.eclipse.leshan.core.observation.Observation;
import org.eclipse.leshan.core.request.ObserveRequest;
import org.eclipse.leshan.core.response.ObserveResponse;
import org.eclipse.leshan.server.californium.LeshanServer;
import org.eclipse.leshan.server.californium.LeshanServerBuilder;
import org.eclipse.leshan.server.observation.ObservationListener;
import org.eclipse.leshan.server.registration.Registration;
import org.eclipse.leshan.server.registration.RegistrationListener;
import org.eclipse.leshan.server.registration.RegistrationUpdate;

import leshanserver.model.Dimmer;
import leshanserver.model.Luminaire;
import leshanserver.model.Piece;
import leshanserver.model.PieceObject;
import leshanserver.model.Sensor;
import leshanserver.model.Switch;

public class MyLeshan extends Thread {

	private static MyLeshan instance = null;
	public static MyLeshan getInstance() {
		if (instance == null) {
			synchronized (MyLeshan.class) {
				if (instance == null) {
					instance = new MyLeshan();
				}
			}
		}
		return instance;
	}

	private int getObjectId(String url) {
		String[] tokens = url.split("/");
		return Integer.parseInt(tokens[1]);
	}

	private int getObjectInstance(String url) {
		String[] tokens = url.split("/");
		return Integer.parseInt(tokens[2]);
	}

	Map<Integer, Piece> pieces = new HashMap<Integer, Piece>();

	public Map<Integer, Piece> getPieces() {
		return pieces;
	}

	@Override
	public void run() {
		this.launch();
	}

	public void launch() {
		LeshanServerBuilder builder = new LeshanServerBuilder();
		LeshanServer server = builder.build();

		server.getObservationService().addListener(new ObservationListener() {
			public void onResponse(Observation obs, Registration reg, ObserveResponse res) {
				int obj_id = obs.getPath().getObjectId();
				int instance_id = obs.getPath().getObjectInstanceId();

				int pieceNumber = instance_id / 1000;

				pieces.get(pieceNumber).update(obj_id, instance_id, res);
			}

			@Override
			public void newObservation(Observation observation, Registration registration) {
				System.out.println("New observation !!!");
			}

			@Override
			public void cancelled(Observation observation) {
				System.out.println("Observation canceled");
			}

			@Override
			public void onError(Observation observation, Registration registration, Exception error) {
				System.err.println("Error linked to observation");
			}
		});

		server.getRegistrationService().addListener(new RegistrationListener() {

			@Override
			public void updated(RegistrationUpdate update, Registration updatedReg, Registration previousReg) {

			}

			@Override
			public void unregistered(Registration registration, Collection<Observation> observations, boolean expired,
					Registration newReg) {
				for (Link link : registration.getObjectLinks()) {
					int obj_id = getObjectId(link.getUrl());
					int inst_id = getObjectInstance(link.getUrl());

					int pieceNumber = inst_id / 1000;
					
					Piece piece = pieces.get(pieceNumber);
					if(piece != null) {
						for(int i = 0 ; i < piece.objects.size() ; i ++) {
							PieceObject po  = piece.objects.get(i);
							if(po.object_id == obj_id && po.instance_id == inst_id) {
								piece.objects.remove(po);
								break;
							}
						}						
					}
				}
			}

			@Override
			public void registered(Registration registration, Registration previousReg,
					Collection<Observation> previousObsersations) {
				for (Link link : registration.getObjectLinks()) {
					int obj_id = getObjectId(link.getUrl());
					int inst_id = getObjectInstance(link.getUrl());

					int pieceNumber = inst_id / 1000;
					

					try {
						PieceObject object = null;

						switch (obj_id) {
						case Sensor.object_id:
							object = new Sensor(inst_id);
							break;
						case Luminaire.object_id:
							object = new Luminaire(inst_id);
							break;
						case Dimmer.object_id:
							object = new Dimmer(inst_id);
							break;
						case Switch.object_id:
							object = new Switch(inst_id);
							break;
						default:
							break;
						}

						if (object == null)
							continue;
						if (!pieces.containsKey(pieceNumber)) {
							pieces.put(pieceNumber, new Piece(server));
						}

						pieces.get(pieceNumber).objects.add(object);

						for (int field : object.getFieldToObserve()) {
							ObserveResponse response = server.send(registration,
									new ObserveRequest(obj_id, inst_id, field));
							if (response.isSuccess()) {
								object.updateValue(response);
							}
						}
						object.registration = registration;

					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});

		server.start();
	}

}

<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ include file="/WEB-INF/jsp/utils/include.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<html>
<head>
<meta charset="UTF-8" />
<title>Index</title>
<link rel="stylesheet" type="text/css" href="/style.css">
<%@ include file="/WEB-INF/jsp/utils/head-bootstrap.jsp"%>
</head>
<body>
	<div class="container middle">
		<h1 style="text-align: center" class="bigTitle">LESHAN</h1>
	</div>
	<c:forEach items="${pieces}" var="piece">
		<div class="card bg-light mb-3 container middle" style="max-width: 40rem; text-align: center;">
		  	<div class="card-header">Pièce n°${piece.key}</div>
		  	<div class="card-body">
		  		<table class="table" style="text-align: center;">
					<thead class="thead-light">
						<tr>
							<th scope="col">EndPoint</th>
							<th scope="col">Name</th>
							<th scope="col">Observed Resource</th>
							<th scope="col">Value</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${piece.value.objects}" var="object">
							
								<c:forEach items="${object.getRowValue()}" var="row">
								<tr>
									<th scope="row">${object.endpointName()}</th>
									<td>${object.ObjectName()}</td>
									<td>${row.key}</td>
									<td>${row.value}</td>
									</tr>
								</c:forEach>
							
						</c:forEach>
					</tbody>
				</table>
		  	</div>
		</div>
	</c:forEach>
</body>
</html>